image: node:current

variables:
  NODE_ENV: "test"
  MYSQL_HOST: "mysql"
  MYSQL_RANDOM_ROOT_PASSWORD: "orsegups"
  MYSQL_ALLOW_EMPTY_PASSWORD: "1"
  MYSQL_DATABASE: "orsegups"
  MYSQL_USER: "orsegups"
  MYSQL_PASSWORD: "orsegups"

.before_script_template: &load_database
  image: klarkc/cypress-mysql-awscli
  services:
  - name: mysql:8.0.16
    alias: mysql
  before_script:
    - mysql --version
    - mysql --user=$MYSQL_USER --password=$MYSQL_PASSWORD --database=$MYSQL_DATABASE --host=$MYSQL_HOST --execute="SHOW DATABASES; ALTER USER '$MYSQL_USER'@'%' IDENTIFIED WITH mysql_native_password BY '$MYSQL_PASSWORD'"
    - npx yarn install --frozen-lockfile --production=false
    - npx yarn workspace @orsegups/gateway run sequelize:migrate
    - npx yarn workspace @orsegups/gateway run sequelize:seed
  tags:
    - has-service
    - heavy-use

before_script:
  - npx yarn install --frozen-lockfile

stages:
  - test
  - build
  - deploy

cache:
  key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
  paths:
    - node_modules/
    - packages/**/node_modules

# Gateway
#gateway:test:lint:
#  except:
#    - schedules
#  stage: test
#  script:
#    - npx yarn workspace @orsegups/gateway run lint
#  tags:
#    - light-use

gateway:test:unit:
  except:
    - schedules
  stage: test
  <<: *load_database
  script:
    - npx yarn workspace @orsegups/gateway run test:unit
  artifacts:
    name: "${CI_JOB_NAME}_${CI_COMMIT_REF_NAME}"
    paths:
      - packages/gateway/coverage
    when: always

gateway:build:
  except:
    - schedules
  stage: build
  script:
    - npx yarn workspace @orsegups/gateway run build
  artifacts:
    name: "${CI_JOB_NAME}_${CI_COMMIT_REF_NAME}"
    paths:
      - packages/gateway/dist
    expire_in: 24 hour
  tags:
    - light-use

gateway:deploy:staging:
  except:
    - schedules
  stage: deploy
  variables:
    NODE_ENV: staging
  <<: *load_database
  environment:
    name: gw_staging
    url: http://api-gateway.us-east-1.elasticbeanstalk.com/
  only:
   - master
  script:
    - ./deploy.sh packages/gateway APi-Gateway gw

gateway:deploy:production:
  except:
    - schedules
  stage: deploy
  variables:
    NODE_ENV: production
  <<: *load_database
  environment:
    name: gw_production
    url: https://api-gateway.orsegups.com.br
  when: manual
  only:
   - master
  script:
    - ./deploy.sh packages/gateway API-GATEWAY gw

gateway:job:staging:
  only:
    - schedules
  variables:
    NODE_ENV: staging
  tags:
    - light-use
  script:
    - npx yarn workspace @orsegups/gateway run job:$JOB

gateway:job:production:
  only:
    - schedules
  variables:
    NODE_ENV: production
  tags:
    - light-use
  script:
    - npx yarn install --frozen-lockfile --production=false
    - npx yarn workspace @orsegups/gateway run job:$JOB

# Portal do Cliente
#portal-cliente:test:lint:
 # except:
  #  - schedules
  #stage: test
  #script:
  #  - npx yarn workspace @orsegups/portal-cliente run lint
  #tags:
  #  - light-use

#portal-cliente:test:unit:
#  except:
#    - schedules
#  stage: test
#  script:
#    - npx yarn workspace @orsegups/portal-cliente run test:unit
#  artifacts:
#    name: "${CI_JOB_NAME}_${CI_COMMIT_REF_NAME}"
#    paths:
#      - packages/portal-cliente/coverage
#    when: always
#  tags:
#    - heavy-use

#portal-cliente:test:e2e:
#  except:
#    - schedules
#  stage: test
 # <<: *load_database
 # script:
 #   - npx yarn workspace @orsegups/portal-cliente run test:e2e:debug
 # cache:
 #   key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
 #   paths:
 #     - packages/**/cache/Cypress
 # variables:
 #   CYPRESS_CACHE_FOLDER: /builds/incuca/clientes/orsegups/packages/portal-cliente/cache/Cypress
 # artifacts:
  #  name: "${CI_JOB_NAME}_${CI_COMMIT_REF_NAME}"
  #  paths:
   #   - packages/portal-cliente/tests/e2e/videos
   #   - packages/portal-cliente/tests/e2e/screenshots
   # expire_in: 2 hour
   # when: always

portal-cliente:build:staging:
  except:
    - schedules
  stage: build
  script:
    - npx yarn workspace @orsegups/portal-cliente run build --dest dist_staging --mode staging
  artifacts:
    name: "${CI_JOB_NAME}_${CI_COMMIT_REF_NAME}"
    paths:
      - packages/portal-cliente/dist_staging
    expire_in: 24 hour
  tags:
    - light-use

portal-cliente:build:production:
  except:
    - schedules
  stage: build
  script:
    - npx yarn workspace @orsegups/portal-cliente run build --dest dist_production --mode production
  artifacts:
    name: "${CI_JOB_NAME}_${CI_COMMIT_REF_NAME}"
    paths:
      - packages/portal-cliente/dist_production
    expire_in: 24 hour
  tags:
    - light-use

portal-cliente:deploy:staging:
  except:
    - schedules
  image: klarkc/node-aws-cli:latest
  stage: deploy
  variables:
    NODE_ENV: staging
  environment:
    name: pc_staging
    url: http://newgate-front.us-east-1.elasticbeanstalk.com/
  only:
   - master
  script:
    - ./deploy.sh packages/portal-cliente NewGateDev-Front pc
  tags:
    - light-use

portal-cliente:deploy:production:
  except:
    - schedules
  image: klarkc/node-aws-cli:latest
  stage: deploy
  variables:
    NODE_ENV: production
  environment:
    name: pc_production
    url: https://novo-portal-prod.sa-east-1.elasticbeanstalk.com/
  when: manual
  only:
   - master
  script:
    - ./deploy.sh packages/portal-cliente Novo-Portal-Orsegups-Front-Prod pc
  tags:
    - light-use
