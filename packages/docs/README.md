# Documentação

Documentação do time orsegups para o Gateway e o Portal do cliente.

> Uma documentação viva da API do Gateway está disponível no endereço [http://localhost:3000/api-docs/](http://localhost:3000/api-docs/) no ambiente de desenvolvimento.

## Servir

`yarn run serve`

## Atualizar usando comentários no código

`yarn run jsdoc`

## Compilar

`yarn run build`