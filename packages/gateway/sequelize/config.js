/* eslint-disable no-console */
/* eslint-disable import/no-extraneous-dependencies */
require('@babel/register');

const path = require('path');
const fs = require('fs');

const NODE_ENV = process.env.NODE_ENV || 'staging';

function getPath(file) {
  return path.resolve(__dirname, `../config/${file}.json`);
}

function readConfig(file) {
  const config = JSON.parse(fs.readFileSync(getPath(file))).database;

  return Object.assign({}, {
    database: config.database,
    username: config.username,
    password: config.password,
    host: config.host,
    dialect: config.dialect || 'mysql',
  });
}

const seqToConfig = {
  development: 'default',
  staging: 'staging',
  production: 'production',
  test: 'test',
};

const config = {
  [NODE_ENV]: readConfig(seqToConfig[NODE_ENV]),
};

// We use env variables on CI
if (process.env.CI_JOB_STAGE === 'test') {
  config[NODE_ENV].database = process.env.MYSQL_PASSWORD;
  config[NODE_ENV].username = process.env.MYSQL_USER;
  config[NODE_ENV].password = process.env.MYSQL_PASSWORD;
  config[NODE_ENV].host = process.env.MYSQL_HOST;
  console.log('running on CI test stage, config was overwritten by env variables');
}

console.log('setting sequelize on', config);
module.exports = config;
