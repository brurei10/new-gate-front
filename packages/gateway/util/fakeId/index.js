/* eslint-disable import/no-extraneous-dependencies */
import express, { json, urlencoded } from '@feathersjs/express';
import basicAuth from 'express-basic-auth';
import user from './__fixtures__/user';

const basicAuthMiddleware = basicAuth({
  users: {
    incuca: 'bGciOiJSUzI1NiIsInR5cCI6IkpXVCJ',
  },
});

function routes(app) {
  app.post('/oauth/token', basicAuthMiddleware, (req, res) => {
    res.status(200).send({
      access_token: 'fakeid-token',
      user,
    });
  });

  app.get('/account/all*', (_, res) => {
    res.status(200).send([]);
  });
}

const app = express();
app.use(json());
app.use(urlencoded({ extended: true }));

routes(app);

const server = app.listen(3030, () => {
  console.log('fakeId running on port.', server.address().port);
});
