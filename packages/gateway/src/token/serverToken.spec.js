import feathers from '@feathersjs/feathers';
import configuration from '@feathersjs/configuration';

import setupServerToken from './serverToken';

describe('serverToken', () => {
  let app;

  beforeAll(() => {
    jest.setTimeout(30000);
  });

  beforeEach(() => {
    app = feathers();
    app.configure(configuration());
    app.jobs = {
      register: jest.fn(),
    };
  });

  it('set serverToken', async () => {
    await setupServerToken(app);
    expect(app.serverToken).toMatchObject({
      access_token: expect.any(String),
      expires_in: expect.any(Number),
      createdAt: expect.any(Date),
    });
  });

  it('register refreshToken job', async () => {
    await setupServerToken(app);
    expect(app.jobs.register).toBeCalledWith(
      'refreshToken',
      expect.any(Function),
    );
  });


  it('dont set serverToken if its already set', async () => {
    const serverToken = {
      access_token: 'foo',
      expires_in: 9000,
      createdAt: new Date(),
    };
    app.serverToken = serverToken;
    await setupServerToken(app);
    expect(app.serverToken).toBe(serverToken);
  });
});
