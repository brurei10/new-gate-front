import { logger } from '@/logger';

/**
 * Persist any token and user in orsegupsID context property
 *
 * @export
 * @memberof module:TokenHooks
 * @typedef module:TokenHooks.persistOrsId
 * @name persistOrsId
 * @param {Context} context
 * @returns {Promise.<Context>} context with result set (if token is persisted)
 */
export default async function persistOrsId(context) {
  if (context.orsegupsID) {
    const {
      app, service, orsegupsID, params,
    } = context;
    logger.info('updating user, token and accounts %s', params.query.username);
    const users = app.service('users');
    const accounts = app.service('accounts');
    const {
      email, cpfCnpj, phone, whatsPhone,
      firstAccess, name, active, id, login,
    } = orsegupsID.user;
    // isAdmin is in Account instead User in orsegups ID, so
    // if gateway has user, use the last isAdmin info
    // otherwise assumes that it's a returning admin user that
    // does not have a gateway registry
    const isAdmin = context.storedUser ? !!context.storedUser.isAdmin : true;

    const accessToken = orsegupsID.access_token;
    const expiresIn = orsegupsID.expires_in;
    // create/update user
    const user = await users.create({
      id,
      name,
      email,
      cpfCnpj,
      phone,
      login,
      whatsPhone,
      password: params.query.password,
      firstAccess,
      active,
      isAdmin,
    });
    // account guest creator
    /* istanbul ignore next */
    const createGuest = features => ({
      id: user.id,
      features,
    });
    /* istanbul ignore next */
    const verifyOwner = acc => users.find({ query: { id: acc.userOwnerId } }).then((res) => {
      if (res.length < 1) {
        const data = {
          id: acc.userOwnerId,
          name: acc.userOwnerName,
          isAdmin: acc.isAdmin,
        };
        logger.debug(
          'userOwner not found on gateway, adding one with %s',
          JSON.stringify(data),
        );
        return users.create(data).then((owner) => {
          logger.debug('userOwner %s added', owner.id);
          return acc;
        });
      }
      logger.debug('userOwner %s already on gateway, skipping insertion', acc.userOwnerId);
      return acc;
    });
    // create/update accounts
    const upsertAccount = accPromise => accPromise.then((account) => {
      // add account guest if account is from another user
      const guest = account.userOwnerId !== user.id ? createGuest(account.features) : undefined;
      logger.debug(
        'upserting account %s with received account and guest %s',
        account.id,
        JSON.stringify(guest),
      );

      return accounts.create({
        id: account.id,
        contractCode: account.contractCode,
        empresaContrato: account.empresaContrato,
        filialContrato: account.filialcontrato,
        isActive: account.isActive,
        userOwnerId: account.userOwnerId,
        cpfCnpjOwner: account.cpfCnpjOwner,
        identifier: account.identifier,
        productName: account.productName,
        name: account.name,
        guest,
      });
    });
    const accs = orsegupsID.accounts;
    await Promise.all(accs.map(verifyOwner).map(upsertAccount));
    // create/update token
    const result = await service.create({
      email,
      accessToken,
      expiresIn,
      userId: user.id,
    }, { skipAuth: true });
    return {
      ...context,
      result: [result],
    };
  }
  logger.debug('skipping persistOrsId');
  return context;
}
