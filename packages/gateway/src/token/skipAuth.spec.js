import { secureSkipAuth, ensureSkipAuth } from './skipAuth';

describe('skipAuth', () => {
  it('set skipAuth from params in express requisitions', (done) => {
    const req = {
      feathers: {
        skipAuth: true,
      },
    };
    secureSkipAuth(req, {}, () => {
      expect(req.feathers.skipAuth).toBeFalsy();
      done();
    });
  });

  it('append skipAuth on context params', () => {
    const context = {
      params: {},
    };
    ensureSkipAuth(context);
    expect(context.params.skipAuth).toBeTruthy();
  });
});
