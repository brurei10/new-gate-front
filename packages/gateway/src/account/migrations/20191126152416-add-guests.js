/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.addColumn('Accounts', 'guests', {
    type: Sequelize.JSON,
    defaultValue: [],
  }),
  down: (QI, Sequelize) => QI.removeColumn('Accounts', 'guests'),
};
