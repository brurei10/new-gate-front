/* eslint-disable no-unused-vars */


module.exports = {
  up: (QI, Sequelize) => QI.sequelize.query(
    'ALTER TABLE Accounts DROP PRIMARY KEY, MODIFY id BIGINT UNSIGNED NOT NULL PRIMARY KEY',
  ),
  down: (QI, Sequelize) => QI.sequelize.query(
    'ALTER TABLE Accounts DROP PRIMARY KEY, MODIFY id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY',
  ),
};
