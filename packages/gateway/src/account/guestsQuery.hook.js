/* istanbul ignore file */
import axios from 'axios';
import { logger } from '@/logger';

async function updateGuestsWithGuest(context, isUpdating = false) {
  const givenGuest = context.data.guest;
  if (isUpdating) {
    const savedAcc = await context.service.get(context.data.id);
    const updtingGuests = [...(savedAcc.guests || [])];
    const curGuestIdx = updtingGuests.findIndex(g => g.id === givenGuest.id);
    if (curGuestIdx > -1) {
      // try to update saved matching guest
      updtingGuests[curGuestIdx] = givenGuest;
    } else {
      // just create guest if was not found
      updtingGuests.push(givenGuest);
    }

    logger.debug(
      'guest found on context data, setting %s on data.guests',
      JSON.stringify(updtingGuests),
    );
    context.data.guests = updtingGuests;
  } else {
    context.data.guests = [givenGuest];
  }
  return context;
}

export default function guestsQuery() {
  return {
    async beforeUpdate(context) {
      const { guest } = context.data;
      if (guest) await updateGuestsWithGuest(context, true);
      return context;
    },
    async beforeCreate(context) {
      const { guest } = context.data;
      if (guest) await updateGuestsWithGuest(context);
      return context;
    },
    async beforePatch(ctx) {
      const { app, params } = ctx;
      if (ctx.data.delete) {
        const orsegupsId = app.get('orsegupsId');
        const { accessToken } = params.clientToken;
        const endpoint = `${orsegupsId.uri}/user/delete`;
        logger.debug('posting %s on orsegups id', JSON.stringify(ctx.data));
        try {
          const ret = await axios.put(
            endpoint,
            {
              userId: ctx.data.user.id,
              accountId: ctx.id,
              isAdmin: ctx.data.user.isAdmin,
            },
            {
              withCredentials: true,
              headers: {
                'Content-Type': 'application/json',
                Authorization: `bearer ${accessToken}`,
              },
            },
          );
          if (ctx.data.user.isAdmin) {
            throw new Error(ret.data.message);
          } else {
            logger.debug(
              'guest %s found and removed from account %s',
              JSON.stringify(ctx.data.user.email),
              JSON.stringify(ctx.id),
            );
            const savedAcc = await ctx.service.get(ctx.id);
            const guests = savedAcc.guests.filter(({ id }) => id !== ctx.data.user.id);
            ctx.data.guests = guests;
            delete ctx.data.delete;
            delete ctx.data.user;
            logger.debug('account %s patched with data %s', JSON.stringify(ctx.id), JSON.stringify(ctx.data));
          }
        } catch (err) {
          logger.error(
            'error removing account %s on orsegups id, details: %s',
            ctx.data.user.email,
          );
          throw err;
        }
      }
      return ctx;
    },
    beforeFind(context) {
      if (context.params.query && context.params.query.guestId) {
        logger.debug(
          'guestId %s found on query, attaching filterByGuestId on context',
          context.params.query.guestId,
        );
        context.$filterByGuestId = context.params.query.guestId;
        delete context.params.query.guestId;
      }
      return context;
    },
    afterFind(context) {
      if (context.$filterByGuestId) {
        logger.debug(
          'filterByGuestId % found on context, filtering accounts to matching guests',
          context.$filterByGuestId,
        );
        context.result = context.result.filter(
          user => user.guests && user.guests.some(
            guest => guest.id === Number(context.$filterByGuestId),
          ),
        );
      }
      return context;
    },
  };
}
