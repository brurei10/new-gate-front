import swagger from 'feathers-swagger';
import swaggerUi from 'swagger-ui-express';

export default function setupSwagger(app) {
  const setup = swagger({
    specs: {
      info: {
        title: 'Orsegups Gateway',
        description: 'Documentação da API Gateway da Orsegups',
        version: process.env.APP_VERSION,
      },
    },
  });
  // console.log(app.get);
  app.configure(setup);
  let uri = `${app.get('host')}`;
  if (process.env.NODE_ENV !== 'staging') {
    uri += `:${app.get('port')}`;
  }
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(null, {
    swaggerOptions: {
      url: `http://${uri}/docs`,
    },
  }));
}
