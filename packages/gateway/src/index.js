import { logger } from './logger';
import app from './app';

const port = app.get('port');
const host = app.get('host');
const server = app.listen(port);

/* istanbul ignore next */
const version = process.env.APP_VERSION ? `(version: ${process.env.APP_VERSION}) ` : '';

process.on('unhandledRejection', (reason, p) => {
  logger.error('Unhandled Rejection at: Promise ', p, reason);
});

server.on('listening', () => {
  logger.info(
    'Gateway %sapplication started on http://%s:%d',
    version,
    host,
    port,
  );
  logger.info('Database', app.get('database'));
  logger.info('ID config', app.get('orsegupsId'));
  logger.info('Portal Mobile config', app.get('portalMobile'));
});
