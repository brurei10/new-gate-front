import logger from './getHookHandler';
import hooks from './hooks';

jest.mock('./getHookHandler', () => ({
  __esModule: true,
  default: jest.fn(() => 'logger'),
}));

describe('logger hooks', () => {
  it('add logger hook', () => {
    expect(logger).toBeCalled();
    expect(hooks.before.all).toEqual(
      expect.arrayContaining(['logger']),
    );
    expect(hooks.after.all).toEqual(
      expect.arrayContaining(['logger']),
    );
    expect(hooks.error.all).toEqual(
      expect.arrayContaining(['logger']),
    );
  });
});
