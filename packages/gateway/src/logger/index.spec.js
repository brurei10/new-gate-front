import express from '@feathersjs/express';
import logger from './winstonLogger';
import setupLogger from '.';

jest.mock('./hooks');
jest.mock('./winstonLogger');
jest.mock('@feathersjs/express', () => ({
  errorHandler: jest.fn(() => 'foo'),
}));

describe('logger', () => {
  const app = {
    hooks: jest.fn(),
    use: jest.fn(),
  };

  it('set hooks', () => {
    setupLogger(app);
    expect(app.hooks).toBeCalled();
  });

  it('use express error handler with winstonLogger', () => {
    setupLogger(app);
    expect(app.use).toBeCalledWith('foo');
    expect(express.errorHandler).toBeCalledWith(
      expect.objectContaining({ logger }),
    );
  });
});
