import url from 'url';
import request from 'request-promise';
import app from './app';

const port = app.get('port') || 3000;
const getUrl = pathname => url.format({
  hostname: app.get('host') || 'localhost',
  protocol: 'http',
  port,
  pathname,
});

describe('app integration', () => {
  let server;

  beforeAll((done) => {
    server = app.listen(port);
    server.once('listening', () => done());
  });

  afterAll((done) => {
    server.close(done);
  });

  it('starts and show hello msg', async () => {
    const res = await request(getUrl(), {
      headers: {
        Origin: 'http://localhost:3000',
      },
    });
    expect(res).toEqual('Hello Node.js Server!');
  });
});
