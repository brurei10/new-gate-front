import feathers from '@feathersjs/feathers';
import configuration from '@feathersjs/configuration';

import Sequelize from 'sequelize';
import { logger } from '@/logger';
import setupDatabase from './database';

jest.mock('sequelize');
jest.mock('@/logger');

describe('database unit', () => {
  describe('setupAssociations', () => {
    let app;

    beforeAll(() => {
      jest.setTimeout(30000);
    });

    beforeEach(() => {
      app = feathers();
      app.configure(configuration());
    });

    it('calls associate for each defined model', () => {
      const spy = jest.fn();
      const models = {
        Foo: {
          associate: spy,
        },
      };
      Sequelize.mockImplementation(
        () => ({ models }),
      );
      setupDatabase(app);
      app.db.setupAssociations();
      expect(logger.debug).toBeCalledWith(
        expect.stringMatching(/%s/),
        'Foo',
      );
      expect(spy).toBeCalledWith(models);
    });

    it('does not call associate if model has not the function', () => {
      const spy = jest.fn();
      const models = {
        Foo: {},
      };
      Sequelize.mockImplementation(
        () => ({ models }),
      );
      setupDatabase(app);
      app.db.setupAssociations();
      expect(spy).not.toBeCalled();
    });
  });
});
