/* istanbul ignore file */

/**
  * MobileRequest domain
  *
  * @module MobileRequest
  */
import MobileRequestService from './mobileRequest.service';

/**
 * Configure mobileRequest domain with services and hooks
 *
 * @name MobileRequestSetup
 * @memberof module:MobileRequest
 * @param {Feathers} app
 */
export default function MobileRequestSetup(app) {
  // Initialize our service with any options it requires
  const mobileRequestService = MobileRequestService(app);

  // Register mobileRequestService
  app.use('/mobileRequests', mobileRequestService);
}
