/* istanbul ignore file */
/* eslint-disable camelcase */
import axios from 'axios';
import uniqid from 'uniqid';
import { logger } from '@/logger';

export default function PortalMobileService(app) {
  return {
    async create(data, params) {
      const cfg = app.get('portalMobile');
      const settings = {
        baseURL: cfg.uri,
        method: 'get',
        headers: {
          'X-AUTH-TOKEN': cfg.accessToken,
          'ORSID-AUTH-TOKEN': params.clientToken.accessToken,
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
        ...data,
      };
      const url = `${settings.baseURL}${axios.getUri(settings)}`;
      logger.debug('sending %s to %s', settings.method, url);
      try {
        const axRes = await axios(settings);
        return {
          id: uniqid(),
          data: axRes.data,
        };
      } catch (err) {
        logger.error(
          'error sending %s to %s, details: %s',
          settings.method,
          url,
          err.response.data,
        );
        throw err;
      }
    },
  };
}
