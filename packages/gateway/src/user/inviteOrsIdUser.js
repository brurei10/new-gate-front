/* istanbul ignore file */
/* eslint-disable camelcase */
import axios from 'axios';
import { logger } from '@/logger';
import OrsegupsIdError from '@/lib/OrsegupsIdError';

export default function () {
  return {
    async beforeCreate(ctx) {
      const { app, params } = ctx;
      if (params.provider) {
      // request comes from outside (REST, socket, etc)
        const orsegupsId = app.get('orsegupsId');
        const { accessToken } = params.clientToken;
        const endpoint = `${orsegupsId.uri}/user/inviteuser`;
        const account = app.service('accounts');
        const attachingAccount = await account.get(ctx.data.accountId);
        const features = (ctx.data.features || []).map(id => ({ id }));

        const patchData = idUser => ({
          ...ctx.data,
          id: idUser.id,
          name: idUser.name,
          email: idUser.email,
          // login allows signin in ors id
          login: idUser.email,
          cpfCnpj: idUser.cpfCnpj,
          phone: idUser.phone,
          whatsPhone: idUser.whatsPhone,
          firstAccess: idUser.firstAccess,
          active: idUser.active,
          // unset to not save in db
          accountId: undefined,
          features: undefined,
        });
        logger.debug('inviting %s on orsegups id', ctx.data.email);
        try {
          const postData = {
            name: ctx.data.name,
            login: ctx.data.email,
            email: ctx.data.email,
            product: {
              id: 2,
              accounts: [{
                id: attachingAccount.id,
                contractCode: attachingAccount.contractCode,
                profiles: [{
                  // orsegups id accept only ids
                  features: features.map(f => f.id),
                }],
              }],
            },
          };
          const postHeaders = {
            withCredentials: true,
            headers: {
              'Content-Type': 'application/json',
              Authorization: `bearer ${accessToken}`,
            },
          };
          const res = await axios.post(
            endpoint,
            postData,
            postHeaders,
          );
          const idUser = res.data.data;
          ctx.$orsIdMessage = res.data.message;
          ctx.data = patchData(idUser);
          logger.debug('user %s invited, details', ctx.data.email, ctx.data);
        } catch (err) {
          const idError = err.response && new OrsegupsIdError(err);
          logger.error(
            'error inviting user %s on orsegups id',
            ctx.data.email,
          );
          throw idError || err;
        }
      }
      return ctx;
    },
    afterCreate({ result, $orsIdMessage }) {
      if ($orsIdMessage) {
        logger.debug('attaching orsegups ID message in response');
        result.$orsIdMessage = $orsIdMessage;
      }
    },
  };
}
