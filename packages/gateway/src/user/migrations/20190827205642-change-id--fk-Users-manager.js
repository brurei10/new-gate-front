/* eslint-disable no-unused-vars */


module.exports = {
  up: (QI, Sequelize) => QI.sequelize.query(
    'ALTER TABLE Users ADD CONSTRAINT Users_managerId_foreign_idx FOREIGN KEY(managerId) REFERENCES Users(id);',
  ),
  down: (QI, Sequelize) => QI.sequelize.query(
    'ALTER TABLE Users DROP FOREIGN KEY Users_managerId_foreign_idx;',
  ),
};
