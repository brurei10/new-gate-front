/* eslint-disable no-unused-vars */


module.exports = {
  up: (QI, Sequelize) => QI.sequelize.query(
    'ALTER TABLE Accounts DROP FOREIGN KEY Contracts_userOwnerId_foreign_idx, MODIFY userOwnerId BIGINT UNSIGNED NOT NULL;',
  ),
  down: (QI, Sequelize) => QI.sequelize.query(
    'ALTER TABLE Accounts ADD CONSTRAINT Contracts_userOwnerId_foreign_idx FOREIGN KEY(userOwnerId) REFERENCES Users(id), MODIFY userOwnerId INT(11) NOT NULL;',
  ),
};
