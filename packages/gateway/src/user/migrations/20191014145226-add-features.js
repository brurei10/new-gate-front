/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.addColumn('Users', 'features', {
    type: Sequelize.JSON,
    defaultValue: [],
  }),
  down: (QI, Sequelize) => QI.removeColumn('Users', 'features'),
};
