import axios from 'axios';
import { logger } from '@/logger';
import patchOrsIdUser from './patchOrsIdUser';
import OrsegupsIdError from '@/lib/OrsegupsIdError';

jest.mock('axios');
jest.mock('@/logger');

describe('patchOrsIdUser hook', () => {
  it('send patch requisition to orsegups id', async () => {
    const data = {
      id: 1,
      name: 'Foo',
      email: 'foo@bar',
      cpfCnpj: '124124',
      phone: '21312412',
      whatsPhone: '21312412',
      firstAccess: true,
    };
    const orsegupsId = {
      uri: 'http://fakeurl',
    };
    const token = 'fakeToken';
    const app = {
      get: jest.fn(() => orsegupsId),
    };
    const params = {
      clientToken: {
        accessToken: token,
      },
      provider: 'rest',
    };
    const endpoint = `${orsegupsId.uri}/user/updateuser`;
    const hook = patchOrsIdUser();
    axios.patch.mockResolvedValue({ data: { data } });
    await hook({ app, data, params });
    expect(app.get).toBeCalledWith('orsegupsId');
    expect(axios.patch).toBeCalledWith(
      endpoint,
      {
        cpfCnpj: data.cpfCnpj,
        email: undefined,
        firstAccess: true,
        id: data.id,
        name: data.name,
        phone: data.phone,
        whatsPhone: data.whatsPhone,
      },
      {
        withCredentials: true,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `bearer ${token}`,
        },
      },
    );
  });

  it('skip if no provider is given', async () => {
    axios.patch.mockClear();
    const app = {};
    const params = {};
    const hook = patchOrsIdUser();
    const ctx = { app, params };
    await hook(ctx);
    expect(axios.patch).not.toBeCalled();
    expect(ctx).toEqual(ctx);
  });

  it('throw if orsegups id error', async () => {
    expect.assertions(4);
    const data = {
      id: 1,
      name: 'Foo',
      email: 'foo@bar',
      cpfCnpj: '124124',
      phone: '21312412',
      whatsPhone: '21312412',
      firstAccess: true,
    };
    const orsegupsId = {
      uri: 'http://fakeurl',
    };
    const token = 'fakeToken';
    const app = {
      get: jest.fn(() => orsegupsId),
    };
    const params = {
      clientToken: {
        accessToken: token,
      },
      provider: 'rest',
    };
    const hook = patchOrsIdUser();
    const postRet = {
      response: {
        data: {
          error: 'ANY_ERROR',
        },
      },
    };
    axios.patch.mockRejectedValue(postRet);
    const ctx = { app, data, params };
    try {
      await hook(ctx);
    } catch (err) {
      expect(err).toBeInstanceOf(OrsegupsIdError);
      expect(err.idError).toBe(postRet.response.data);
      expect(ctx.data).toBe(data);
      expect(logger.error).toBeCalledWith(
        expect.stringMatching(/%s/),
        data.id,
      );
    }
  });
});
