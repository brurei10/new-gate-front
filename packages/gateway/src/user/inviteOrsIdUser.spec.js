import axios from 'axios';
import feathers from '@feathersjs/feathers';
import configuration from '@feathersjs/configuration';

import { logger } from '@/logger';
import OrsegupsIdError from '@/lib/OrsegupsIdError';
import inviteOrsIdUser from './inviteOrsIdUser';

jest.mock('axios');
jest.mock('@/logger');

describe('inviteOrsIdUser hook', () => {
  let app;

  beforeAll(() => {
    jest.setTimeout(30000);
  });

  beforeEach(() => {
    app = feathers();
    app.configure(configuration());
  });

  describe('beforeCreate', () => {
    it('send post requisition to orsegups id and update ctx data', async () => {
      const data = {
        id: 1,
        name: 'Foo',
        email: 'foo@bar',
        cpfCnpj: '124124',
        phone: '21312412',
        whatsPhone: '21312412',
        firstAccess: true,
        accountId: 10,
        managerId: 20,
      };
      const token = 'fakeToken';
      const params = {
        clientToken: {
          accessToken: token,
        },
        provider: 'rest',
      };
      const orsegupsId = app.get('orsegupsId');
      const endpoint = `${orsegupsId.uri}/user/inviteuser`;
      const hook = inviteOrsIdUser().beforeCreate;
      const account = {
        id: data.accountId,
        contractCode: 'SAPIENS',
      };
      app.use('/accounts', {
        get: () => Promise.resolve(account),
      });
      const postData = {
        name: data.name,
        login: data.email,
        email: data.email,
        product: {
          id: 2,
          accounts: [{
            id: account.id,
            contractCode: account.contractCode,
            profiles: [{
              features: [],
            }],
          }],
        },
      };
      const postRetData = {
        ...postData,
        id: 1,
        cpfCnpj: '1234',
        phone: '999999',
        whatsPhone: '999999',
        firstAccess: false,
        active: true,
      };
      axios.post.mockResolvedValue({
        data: {
          data: postRetData,
        },
      });
      const ctx = { app, data, params };
      await hook(ctx);
      expect(axios.post).toBeCalledWith(
        endpoint,
        postData,
        {
          withCredentials: true,
          headers: {
            'Content-Type': 'application/json',
            Authorization: `bearer ${token}`,
          },
        },
      );
      expect(ctx.data).toEqual({
        ...data,
        id: postRetData.id,
        name: postRetData.name,
        email: postRetData.email,
        login: postRetData.email,
        cpfCnpj: postRetData.cpfCnpj,
        phone: postRetData.phone,
        whatsPhone: postRetData.whatsPhone,
        firstAccess: postRetData.firstAccess,
        active: postRetData.active,
        accountId: undefined,
        features: undefined,
      });
    });

    it.skip('append $orsIdMessage in context', async () => {
      // TODO: Add missing test
    });

    it('skip if no provider is given', async () => {
      axios.post.mockClear();
      const params = {};
      const hook = inviteOrsIdUser().beforeCreate;
      const ctx = { app, params };
      await hook(ctx);
      expect(axios.post).not.toBeCalled();
      expect(ctx).toEqual(ctx);
    });

    it('throw if orsegups id error', async () => {
      expect.assertions(3);
      const data = {
        id: 1,
        name: 'Foo',
        email: 'foo@bar',
        cpfCnpj: '124124',
        phone: '21312412',
        whatsPhone: '21312412',
        firstAccess: true,
        accountId: 10,
        managerId: 20,
      };
      const token = 'fakeToken';
      const params = {
        clientToken: {
          accessToken: token,
        },
        provider: 'rest',
      };
      const hook = inviteOrsIdUser().beforeCreate;
      const account = {
        id: data.accountId,
        contractCode: 'SAPIENS',
      };
      app.use('/accounts', {
        get: () => Promise.resolve(account),
      });
      const postRet = {
        response: {
          data: {
            error: 'ANY_ERROR',
          },
        },
      };
      axios.post.mockRejectedValue(postRet);
      const ctx = { app, data, params };
      try {
        await hook(ctx);
      } catch (err) {
        expect(err).toBeInstanceOf(OrsegupsIdError);
        expect(ctx.data).toBe(data);
        expect(logger.error).toBeCalledWith(
          expect.stringMatching(/%s/),
          data.email,
        );
      }
    });
  });

  describe('afterCreate', () => {
    it.skip('append orsegups ID message on result', () => {
      // TODO: inviteOrsIdUser afterCreate hook tests
    });
  });
});
