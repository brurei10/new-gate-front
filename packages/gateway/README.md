# gateway

> [API Docs](http://api-gateway.us-east-1.elasticbeanstalk.com/api-docs/)

## Arquitetura

```
--------------------------------------------------------
| Aplicação                                            |
| ---------------------------------------------------- |
| | Domínio                                          | |
| | ------------------------------------------------ | |
| | | Middleware <= Service <= Adapter <= Database | | |
| | |               Hooks                          | | |
| | ------------------------------------------------ | |
| ---------------------------------------------------- |
--------------------------------------------------------
```

A camada de aplicação é mantida pelo Express.

Cada Domínio do sistema pode possuir toda a arquitetura de forma auto-contida.

Cada diretório da pasta `src` representa um domínio. Dentro de cada domínio os arquivos são pós-fixados com:

- `index` (middleware): Configuração do serviço e encaminhamento de rotas
- `.service` (service): Lógica de negócio e entidades
- `.hooks` (hooks): Gatilhos para lógica entre as operações dos serviços
- `.model` (adapter): Interação entre entidade e banco de dados
- `.doc` (documentação): Documentação do Swagger

O Database é unificado em um único arquivo `db.json` na pasta raiz.

## Project setup

```bash
yarn install
```

Revise as configurações do banco de dados em `config/default.json` e `config/test.json`.

Crie o banco de dados (caso ainda não tenha o feito):

```bash
yarn run sequelize:create
```

## Development server

```
yarn run serve
DEBUG_LEVEL='debug' yarn run serve
```

## Build

```
yarn build
```

## Test

```
yarn run test
```

> Testes de integração terminam em `.test.js`.

> Testes unitários terminam em `.spec.js`.

## Lint

```
yarn run lint
```

## Database

Compatibile with MySQL server >= 8

### Local
Configurações em `config/`, para criar um usuário e database orsegups:

```sql
CREATE USER 'orsegups'@'localhost' IDENTIFIED BY 'orsegups';
CREATE DATABASE orsegups;
GRANT ALL PRIVILEGES ON orsegups.* TO 'orsegups'@'localhost';
FLUSH PRIVILEGES;
```

### Docker Database Container

```bash
yarn serve:db
```

## Migrations

```bash
cd pasta/do/dominio
mkdir migrations seeders
CWD=$(pwd) yarn sequelize argumentos
```

### Onde `argumentos` podem ser:

#### Para criar nova model com a primeira migration

```
model:generate --name NomeDaModel --attributes nomeAtributo:tipo
```

#### Para criar uma nova migration

```
migration:create --name="meaningful-name"
```

#### Aplicar migration do domínio

```
db:migrate
```

#### Gerar seed do domínio

```
seed:generate --name demo-user
```

#### Aplicar seeds do domínio

```
db:seed:all
```

#### Mais comandos

> [Migrations Workflow](https://github.com/feathersjs-ecosystem/feathers-sequelize#migrations-workflow)

### Criar/Recriar banco de dados

```bash
yarn run sequelize:drop
yarn run sequelize:create
```

### Executar as migrations de todos os domínios

```bash
yarn run sequelize:migrate
```

### Executar os seeds de todos os domínios

```bash
yarn run sequelize:seed
```

## Problemas comuns

### ERROR: Client does not support authentication protocol requested by server; consider upgrading MySQL client

Executar SQL, como root:

```sql
ALTER USER 'orsegups' IDENTIFIED WITH mysql_native_password BY 'orsegups';
```