describe('Gateway e2e', () => {
  it('Calls gateway root', () => {
    const url = Cypress.env('VUE_APP_GATEWAY_URI');
    cy.server();
    cy.route(url).as('getGateway');
    cy.visit('/about');
    cy.wait('@getGateway')
      .its('response.body')
      .should('equal', 'Hello Node.js Server!');
  });
});
