function testPattern(pattern, error = false) {
  return v => (v && pattern.test(v) ? true : error);
}

export function required() {
  return value => !!value || 'Esse campo é obrigatório.';
}

export function minSelect(items) {
  return v => (v && v.length >= items ? true : `Mínimo ${items} itens`);
}

export function min(chars) {
  return v => (v && v.length >= chars ? true : `Mínimo ${chars} caracteres`);
}

export function max(chars) {
  return v => (v && v.length < chars ? true : `Máximo ${chars} caracteres`);
}

export function email() {
  const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return testPattern(pattern, 'E-mail inválido.');
}

export function username() {
  const pattern = /[a-zA-Z0-9]+/;
  return testPattern(pattern, 'Usuário inválido');
}

export function cpfCnpj() {
  const pattern = /([0-9]{2}[.]?[0-9]{3}[.]?[0-9]{3}[/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[.]?[0-9]{3}[.]?[0-9]{3}[-]?[0-9]{2})/;
  return testPattern(pattern, 'CPF ou CNPJ inválido');
}

export function match(target, targetName) {
  return v => (v && v === target ? true : `${targetName} não conferem`);
}
