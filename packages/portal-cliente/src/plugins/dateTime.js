import Vue from 'vue';

const DateTime = {
  install(vue) {
    vue.prototype.$dateTime = (date, { time = false, seconds = false } = {}) => {
      const options = {
        day: 'numeric',
        month: 'numeric',
        year: 'numeric',
      };
      if (!time) {
        options.hour = 'numeric';
        options.minute = 'numeric';
      }
      if (!seconds) {
        options.second = 'numeric';
      }
      const dateTime = Intl.DateTimeFormat([], options);
      return dateTime.format(date);
    };
  },
};

Vue.use(DateTime);
