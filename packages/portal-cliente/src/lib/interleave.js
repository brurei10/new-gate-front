/**
 * Join together two arrays, interleaving each value
 *
 * @export
 * @param {Array} arr1
 * @param {Array} arr2
 * @returns interleaved array
 */
export default function interleave(arr1, arr2) {
  const biggerOne = arr1.length > arr2.length ? arr1 : arr2;
  const lesserOne = arr1.length >= arr2.length ? arr2 : arr1;
  return biggerOne
    .reduce((rArr, bVal, bIdx) => ([
      ...rArr,
      ...lesserOne[bIdx] ? [lesserOne[bIdx], bVal] : [bVal],
    ]), []);
}
