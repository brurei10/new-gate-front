export function isObject(item) {
  return (item && typeof item === 'object' && !Array.isArray(item));
}

export function isArray(item) {
  return (item && typeof item === 'object' && Array.isArray(item));
}

export default function mergeDeep(target, source, mergeArray = false) {
  const output = Object.assign({}, target);
  if (isObject(target) && isObject(source)) {
    Object.keys(source).forEach((key) => {
      const assign = () => {
        Object.assign(output, { [key]: source[key] });
      };
      const replace = () => {
        output[key] = mergeDeep(target[key], source[key], mergeArray);
      };
      const doMerge = () => {
        if (!(key in target)) {
          assign();
        } else {
          replace();
        }
      };
      if (isObject(source[key])) {
        doMerge();
      } else if (mergeArray && isArray(source[key])) {
        doMerge();
      } else {
        assign();
      }
    });
  }
  return output;
}
