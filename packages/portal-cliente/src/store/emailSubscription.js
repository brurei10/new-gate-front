/* istanbul ignore file */
const state = {
  updatingSubscription: false,
  visibleDialog: null,
  emailDialogFormValid: false,
};
const mutations = {
  setUpdatingSubscription(st, updating) {
    st.updatingSubscription = updating;
  },
  setVisibleDialog(st, dialogName) {
    st.visibleDialog = dialogName;
  },
  setEmailDialogFormValid(st, valid) {
    st.emailDialogFormValid = valid;
  },
};
const getters = {
  emailSubscription(st, gt, rSt) {
    const { currentAccount, currentUser } = rSt.session;
    return currentAccount && currentUser && currentUser.emailSubscriptions.find(
      ({ numcpf }) => numcpf === currentAccount.cpfCnpjOwner,
    );
  },
  isEmailSubscribed(st, gt) {
    return !!gt.emailSubscription;
  },
};
const actions = {
  async createSubscription({ commit, dispatch, rootState }, email) {
    commit('setUpdatingSubscription', true);
    const { currentUser, currentAccount } = rootState.session;
    try {
      await dispatch('emailSubscriptions/create', {
        userId: currentUser.id,
        numcpf: currentAccount.cpfCnpjOwner,
        email,
      }, { root: true });
    } catch (e) {
      this.$notify('Erro ao se inscrever, por favor contate o nosso suporte.');
    } finally {
      commit('setUpdatingSubscription', false);
      commit('setVisibleDialog', null);

      // reload currentUser
      dispatch('session/loadUser', null, { root: true });
    }
  },
  async removeSubscription({
    commit, dispatch, rootState, getters: gt,
  }) {
    commit('setUpdatingSubscription', true);
    const { currentUser } = rootState.session;
    try {
      await dispatch('emailSubscriptions/remove', {
        userId: currentUser.id,
        numcpf: gt.emailSubscription.numcpf,
        email: gt.emailSubscription.email,
      }, { root: true });
    } catch (e) {
      this.$notify('Erro ao se desinscrever, por favor contate o nosso suporte.');
    } finally {
      commit('setUpdatingSubscription', false);
      commit('setVisibleDialog', null);

      // reload currentUser
      dispatch('session/loadUser', null, { root: true });
    }
  },
};
export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
